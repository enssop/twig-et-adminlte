<?php

require 'header.php';

// on récupère le nombre d'étudiants
$etudiants = $bdd->prepare("SELECT * FROM etudiant");
$etudiants->execute();
$nbEtudiant = $etudiants->rowcount();

// rendu de la page (on passe toute les variables au moteur de rendu)
echo $twig->render('dashbord.html.twig', [
    'page' => [
        'title' => 'Admin Enssop - Tableau de bord',
    ],
    'nbEtudiant' => $nbEtudiant,
]);
