<?php

// on importe les classes du vendor
require_once 'vendor/autoload.php';

// import des parametres de connexion à la base de données
require_once 'config.php';

// configuration de Twig
$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

// connexion à la base de données
try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo 'problème d\'accès à la base de données';
    exit;
}
